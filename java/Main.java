
	
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;


public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
			
			primaryStage.setTitle("Projet de Synthese : Algorithme de de Dijkstra");
			
			FXMLLoader loader = new
					FXMLLoader(getClass().getResource("ViewFenetre.fxml"));
			
			ControllerFenetre conFen =
					new ControllerFenetre(primaryStage);
			
			loader.setController(conFen);
			
			Parent root = loader.load();
			
		
			Scene scene = new Scene(root,1380,900);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}