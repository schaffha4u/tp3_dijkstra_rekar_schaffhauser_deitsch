

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.RadioButton;

public class ControllerFenetre {

	BufferedReader lecteurAvecBuffer = null;
    String ligne;
	
	@FXML private ToggleGroup typeTas;
    @FXML private Pane _drawZone;
    @FXML private ChoiceBox<String> _choixDepart;
    @FXML private Button _Button_calcul;
    
    private int rayon_graphe = 410, x_centre = 550, y_centre = 470; 
    private double rayon_sommet = 5;
    
	private File inputFile;
	private String outputFile;
	private Stage stage;
	private int nb_sommets, nb_routes;
	private String villeDepart;
	
	private ArrayList<Sommet> listSommets = new ArrayList<Sommet>();
	
	public ControllerFenetre(Stage stage) {
		this.stage = stage;
	}

	@FXML 
	public void initialize(){
		_Button_calcul.setDisable(true);
	}
	
	public void choisirFichier(ActionEvent e) throws NumberFormatException, IOException {
		FileChooser fileChooser = new FileChooser();
		fileChooser.getExtensionFilters().addAll(
				new ExtensionFilter("Fichiers Texte (.txt)", "*.txt"), 
				new ExtensionFilter("Tout", "*.*"));
		
		inputFile = fileChooser.showOpenDialog(stage);
		if (inputFile != null) {
			_choixDepart.getItems().clear();
			System.out.println("FICHIER " + inputFile.getPath() + " charg�!");
			drawGraphe();
		}
	}
	
	public void drawGraphe() throws NumberFormatException, IOException {
		if (inputFile == null) 
			System.out.println("Fichier impossible � utiliser!");
		else {
			_Button_calcul.setDisable(false);
			_drawZone.getChildren().clear();

			listSommets.clear();
			lecteurAvecBuffer = new BufferedReader(
				    new InputStreamReader(new FileInputStream(inputFile),"UTF-8"));
		    
			ligne = lecteurAvecBuffer.readLine();
			
			nb_sommets = Integer.parseInt(ligne);
			
			for (int i = 0; i < nb_sommets; i++) {
				ligne = lecteurAvecBuffer.readLine();
				
				double x = rayon_graphe * Math.cos(2*Math.PI*i/nb_sommets) + x_centre ;
				double y = rayon_graphe * Math.sin(2*Math.PI*i/nb_sommets) + y_centre;
				Circle c = new Circle(x, y, rayon_sommet);
				
				Sommet s = new Sommet(ligne, c);
				
				listSommets.add(s);
				
				s.getLabel().setLayoutX(x);
				s.getLabel().setLayoutY(y - rayon_sommet - 25);
				s.getLabel().setFont(new Font("Arial", 15));
				
				_drawZone.getChildren().addAll(c, s.getLabel());//On ajoute le nom de la ville aux choix pour le départ
				
				_choixDepart.getItems().add(ligne);
			}

		    _choixDepart.getSelectionModel().select(listSommets.get(0).getName());
			nb_routes = Integer.parseInt(lecteurAvecBuffer.readLine());
			
			for (int i = 0; i < nb_routes; i++) {
				ligne = lecteurAvecBuffer.readLine();
				int s1 = ligne.indexOf("(");
				int s2 = ligne.indexOf(")");
				
				ligne = ligne.substring(s1 + 1 , s2 - 1);
				String[] spl = ligne.split(",");
				
				for(Sommet s : listSommets) {
					if (s.getName().contentEquals(spl[0].trim())) {//Si on est sur la ville d'origine de la route
						for (Sommet _s : listSommets) {
							if (_s.getName().contentEquals(spl[1].trim())) {
								Segment seg = new Segment(s, _s);
								s.add(seg);
								_s.add(seg);
								_drawZone.getChildren().add(seg.getLigne());
							}
						}
					}
				}
			}
		}  
	}
	
	public void calculer(ActionEvent e) throws IOException{
		if (inputFile == null) {
			System.out.println("Veuillez d'abord choisir un fichier!");
		}
		else {
			resetCalculs();

			villeDepart = _choixDepart.getValue();
			outputFile = inputFile.getParent() + "/out";
			RadioButton selectedRadioButton = (RadioButton) typeTas.getSelectedToggle();

			if (selectedRadioButton.getText().contentEquals("Tableau Dynamique"))
				new MyClass().appelDijkstra(inputFile.getAbsolutePath(), outputFile, villeDepart, 0);

			else if (selectedRadioButton.getText().contentEquals("Arbre Binaire"))
				new MyClass().appelDijkstra(inputFile.getAbsolutePath(), outputFile, villeDepart, 1);
			
			else if (selectedRadioButton.getText().contentEquals("Liste ordonnée"))
				new MyClass().appelDijkstra(inputFile.getAbsolutePath(), outputFile, villeDepart, 2);
			
			else{
				System.out.println("erreur checkboxxxxxxxxxxxxxx");
				return;
			}
			
			lecteurAvecBuffer = new BufferedReader(
				    new InputStreamReader(new FileInputStream(outputFile),"UTF-8"));
			
			ligne = lecteurAvecBuffer.readLine();
			ligne = lecteurAvecBuffer.readLine(); //La ville de d�part

			for (Sommet s : listSommets) {//On entoure le d�part en rouge
				if (s.getName().contentEquals(ligne)) {
					s.getCercle().setFill(Color.RED);
					s.getLabel().setTextFill(Color.RED);
					break;
				}
			}
			
			for (int i = 0; i < nb_sommets - 1; i++) {//On mets � jour les pred et les dist
				ligne = lecteurAvecBuffer.readLine();
				String[] spl = ligne.split(" ");
				
				for (Sommet s : listSommets) {
					if (s.getName().contentEquals(spl[0])) {
						s.getLabel().setText(s.getName() + " (" + spl[1] + "km)");
						for (Segment seg : s.getAlist()) {
							if (seg.getA().getName().contentEquals(spl[2]) || seg.getB().getName().contentEquals(spl[2])) {//Si on a trouv� le segment liant ville1 � vill2 (ici spl[0] � spl[2]
								seg.getLigne().setStroke(Color.RED);
								break;
							}
							
						}
						break;
					}
				}
			}
		}
	}
	
	public void quitter() {
		Platform.exit();
	}
	
	public void resetCalculs(){//lors d'un nouveau calcul, remets les routes à 0, les labels à juste le nom et la ville de départ en noir
		for(Sommet s : listSommets){
			if (s.getCercle().getFill().equals(Color.RED)){
				s.getCercle().setFill(Color.BLACK);
				s.getLabel().setTextFill(Color.BLACK);
			}

			for(Segment seg : s.getAlist()){
				if (seg.getLigne().getStroke().equals(Color.RED))
					seg.getLigne().setStroke(Color.BLACK);
			}
		}
	}

	public void reinitGraph(){
		_drawZone.getChildren().clear();
		_Button_calcul.setDisable(true);
		_choixDepart.getItems().clear();
	}
}
