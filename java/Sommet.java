

import java.util.ArrayList;
import java.util.Collection;

import javafx.scene.control.Label;
import javafx.scene.shape.Circle;

public class Sommet{
	private String name;
	private Label label;
	private Circle cercle;
	private ArrayList<Segment> alist = new ArrayList<Segment>();
	private Sommet pred;
	private int dist;
	
	public Sommet(String name) {
		label = new Label(name);
		setName(name);
		cercle = new Circle();
	}
	
	public Sommet(String name, Circle c) {
		label = new Label(name);
		setName(name);
		setCercle(c);
	}

	public Label getLabel() {
		return this.label;
	}
	
	@Override
	public String toString() {
		return "Sommet [name=" + name + "]";
	}

	public int size() {
		return alist.size();
	}

	public boolean isEmpty() {
		return alist.isEmpty();
	}

	public boolean contains(Object o) {
		return alist.contains(o);
	}

	public Segment get(int index) {
		return alist.get(index);
	}

	public boolean add(Segment e) {
		return alist.add(e);
	}

	public void clear() {
		alist.clear();
	}

	public boolean addAll(Collection<? extends Segment> c) {
		return alist.addAll(c);
	}

	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Sommet other = (Sommet) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	
	public void setX(double x) {
		cercle.setCenterX(x);
	}
	
	public void setY(double y) {
		cercle.setCenterX(y);
	}
	
	public double getX() {
		return cercle.getCenterX();
	}
	
	public double getY() {
		return cercle.getCenterY();
	}
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Circle getCercle() {
		return cercle;
	}

	public void setCercle(Circle cercle) {
		this.cercle = cercle;
	}

	public Sommet getPred() {
		return pred;
	}

	public void setPred(Sommet pred) {
		this.pred = pred;
	}

	public int getDist() {
		return dist;
	}

	public void setDist(int dist) {
		this.dist = dist;
	}

	public ArrayList<Segment> getAlist() {
		return alist;
	}

	public void setAlist(ArrayList<Segment> alist) {
		this.alist = alist;
	}
}
