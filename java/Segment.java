

import javafx.scene.shape.Line;

public class Segment {
	private Line ligne;
	
	private Sommet a, b;
	
	public Segment(Sommet a, Sommet b) {
		setA(a);
		setB(b);
		ligne = new Line(a.getX(), a.getY(), b.getX(), b.getY());
	}
	
	public Segment(Sommet a, Sommet b, Line l) {
		setA(a);
		setB(b);
		setLigne(l);
	}
	
	public Line getLigne() {
		return ligne;
	}

	public void setLigne(Line ligne) {
		this.ligne = ligne;
	}

	public Sommet getA() {
		return a;
	}

	public void setA(Sommet a) {
		this.a = a;
	}

	public Sommet getB() {
		return b;
	}

	public void setB(Sommet b) {
		this.b = b;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Segment other = (Segment) obj;
		
		if (a.equals(other.getA()) && b.equals(other.getB())) return true;
		if (a.equals(other.getB()) && b.equals(other.getA())) return true;
		return true;
	}

	@Override
	public String toString() {
		return "Segment [ligne=" + ligne + ", a=" + a + ", b=" + b + "]";
	}
	
	
	
}
