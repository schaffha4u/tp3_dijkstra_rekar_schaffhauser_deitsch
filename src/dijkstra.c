#include "dijkstra.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/**
 * Algorithme de Dijkstra
 * inFileName : nom du fichier d'entrée
 * outFileName : nom du fichier de sortie
 * sourceName : nom de la ville de départ
 * heaptype : type du tas {0--tableaux dynamiques, 1--arbres binaires complets, 2--listes ordonnées}
 */
void Dijkstra(char * inFileName, char * outFileName, char * sourceName, int heaptype) {
	clock_t start, end;
	double cpu_time_used;

	graph G = readmap(inFileName);
	Heap * H = newHeap(heaptype);

	LNode * iterator = G->head;
	bool start_found = false;
	while(iterator){//On initialise dist de S à zero et on remplit le tas
		struct town * T = iterator->data;
		if (!start_found && strcmp(getTownName(T), sourceName) == 0){//Comme le && paresseux, si on a déjà trouvé le départ on ne fait pas le strcmp (gain de temps)
			setDist(T, 0);
			start_found = true;//Si on a trouvé la ville d'origine, on ne regarde plus les autres car leur dist et pred sont déjà respectivement a infini et null
		}
		T->ptr = H->HeapInsert(H, getDist(T), T);

		iterator = iterator->suc;
	}

	start = clock();

	HNode * u;
	while ((u = H->HeapExtractMin(H)) != NULL){
		struct town * T = u->data;
		LNode * alistIterator = T->alist->head;//iterateur sur la liste de routes la ville T

		while (alistIterator){
			struct road * R = alistIterator->data;
			struct town * voisin;

			if (getURoad(R) == T)//Si le voisin est le V de la route
				voisin = getVRoad(R);
			else//Si le voisin est le U de la route
				voisin = getURoad(R);

			if (getDist(voisin) > (getDist(T) + getKM(R)) ){
				setDist(voisin, getDist(T) + getKM(R));
				setPred(voisin, T);
				
				
				H->HeapIncreasePriority(H, voisin->ptr, getDist(voisin));
			 
			}

			alistIterator = alistIterator->suc;
		}
    }

	end = clock();

	cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
	printf("Dijkstra time = %lf\n", cpu_time_used);

    saveDijkstra(G, outFileName, sourceName);
}

void saveDijkstra(graph G, char * outFileName, char * sourceName){
    FILE * ptrF = fopen(outFileName, "w");

    if (ptrF != NULL){
        fprintf(ptrF, "%d\n", G->numelm);
        fprintf(ptrF, "%s\n", sourceName);
        LNode * iter= G->head;
        while(iter){
            
            struct town * T = iter->data;
            if (strcmp(getTownName(iter->data), sourceName) != 0){
                fprintf(ptrF, "%s %d %s\n", getTownName(T), getDist(T), getTownName(getPred(T)));
                
            }
            iter= iter->suc;
        }
        fclose(ptrF);
    }
    else
    {
        printf("ERREUR : Fichier de sortie introuvable!\n");
    }
    
}

void viewSolution(graph G, char * sourceName) {
	printf("Distances from %s\n", sourceName);
	LNode * iterator;
	for (iterator = G->head; iterator; iterator = iterator->suc) {
		if (strcmp(getTownName(iterator->data), sourceName) != 0)
			printf("%s : %d km (via %s)\n", getTownName((struct town *) iterator->data),
											((struct town *) iterator->data)->dist,
											getTownName(((struct town *) iterator->data)->pred));
	}
}
