#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "dyntable.h"
#include "tree.h"
#include "list.h"
#include "heap.h"
#include "town.h"
#include <stdbool.h>
#include <math.h>

HNode * newHNode(int value, void *data) {
	HNode * Node = (HNode*)calloc(1, sizeof(HNode));

    Node->value = value;
    Node->data = data;
    Node->ptr = NULL;

    return Node;
}

// type =
//    0 (DynTableHeap)
//    1 (CompleteBinaryTreeHeap)
//    2 (ListHeap)
Heap * newHeap(int type) {
	assert(type == 0 || type == 1 || type == 2);
	Heap* H = calloc(1, sizeof(Heap));
	H->dict = newDTable();
	switch (type) {
		case 0:
			H->heap = newDTable();
			H->HeapInsert = DTHeapInsert;
			H->HeapExtractMin = DTHeapExtractMin;
			H->HeapIncreasePriority = DTHeapIncreasePriority;
			H->viewHeap = viewDTHeap;
			break;
		case 1:
			H->heap = newCBTree();
			H->HeapInsert = CBTHeapInsert;
			H->HeapExtractMin = CBTHeapExtractMin;
			H->HeapIncreasePriority = CBTHeapIncreasePriority;
			H->viewHeap = viewCBTHeap;
			break;
		case 2:
			H->heap = newList();
			H->HeapInsert = OLHeapInsert;
			H->HeapExtractMin = OLHeapExtractMin;
			H->HeapIncreasePriority = OLHeapIncreasePriority;
			H->viewHeap = viewOLHeap;
			break;
	}
	return H;
}

/**********************************************************************************
 * DYNAMIC TABLE HEAP
 **********************************************************************************/

void* DTHeapInsert(Heap *H, int value, void *data) {
	DTable * dynTable = H->heap;
	DTable * dict = H->dict;

	HNode * newhNode = newHNode(value, data);

	DTableInsert(dynTable, newhNode);
	int iterator = dynTable->used - 1;//L'indice où l'on vient d'ajouter newHNode dans dynTable
	
	DTableInsert(dict, iterator);//On ajoute au dictionnaire une case avec un pointeur vers le hnode

	int * i = dict->used - 1;
	newhNode->ptr = i;//puis on fait pointer la hnode vers cette case pour toute la durée du programme

	while (iterator != 0 && value < ((HNode*)dynTable->T[(iterator - 1)/2])->value){
		DTableSwap(dynTable, iterator, (iterator-1)/2);

		DTableSwap(H->dict, ((int*)((HNode*)dynTable->T[iterator])->ptr), ((int *) ((HNode*)dynTable->T[(iterator-1)/2])->ptr));

		iterator = (iterator-1)/2;

	}

	return newhNode->ptr;
}

HNode * DTHeapExtractMin(Heap* H) {
	DTable * dynTable = H->heap;

	if (dynTable->used == 0) return NULL;

	DTableSwap(dynTable, 0, (dynTable->used) - 1);
	DTableSwap(H->dict, ((int*)((HNode*)dynTable->T[0])->ptr), ((int *) ((HNode*)dynTable->T[(dynTable->used) - 1])->ptr));


	HNode * last = DTableRemove(dynTable);

	if (dynTable->used > 1){// Si + de 1 élément on déplace le nouveau root, sinon rien à faire
		int i = 0;

		while ((2*i + 1) < (dynTable->used)){//Tant qu'il y a au moins un fg, on peut comparer
			HNode * iterator = dynTable->T[i];

			int i_fg = 2*i + 1;
			int i_fd = i_fg + 1;

			HNode * hnodeFG = dynTable->T[i_fg];

			if (i_fd < dynTable->used){//Donc si il existe un fd également
				HNode * hnodeFD = dynTable->T[i_fd];

				if (hnodeFG->value <= hnodeFD->value && iterator->value > hnodeFG->value){//Si il faut échanger avec le fg
					DTableSwap(dynTable, i_fg, i);	
					DTableSwap(H->dict, ((int*)((HNode*)dynTable->T[i_fg])->ptr), ((int *) ((HNode*)dynTable->T[i])->ptr));
					i = i_fg;
				}
				else if (hnodeFG->value > hnodeFD->value && iterator->value > hnodeFD->value){//Si il faut échanger avec le fd
					DTableSwap(dynTable, i_fd, i);
					DTableSwap(H->dict, ((int*)((HNode*)dynTable->T[i_fd])->ptr), ((int *) ((HNode*)dynTable->T[i])->ptr));
					i = i_fd;
				}
				else break;

			}
			else{//Donc que le fg
				if (iterator->value > hnodeFG->value){//On échange que si il faut, sinon break
					DTableSwap(dynTable, i_fg, i);
					DTableSwap(H->dict, ((int*)((HNode*)dynTable->T[i_fg])->ptr), ((int *) ((HNode*)dynTable->T[i])->ptr));
					i = i_fg;
				}
				else break;
			}
		}
	}

	return last;
}

void DTHeapIncreasePriority(Heap* H, void *position, int value) {
	
	DTable * dynTable = H->heap;
	DTable * dico = H->dict;
	
	int i = ((int *)position); //On case position en entier
	i = dico->T[i]; //Puis on récupère l'index auquel se situe la hnode à modifier dans dynTable
	
	
	HNode * changedHnode = dynTable->T[i];
	changedHnode->value = value; 

	int i_parent = floor((i - 1)/2.0), 
		i_fg = 2*i + 1, 
		i_fd = i_fg + 1;

	HNode * hnodeParent,
		  * hnodeFG,
		  * hnodeFD;

	if (i > 0){//Si il existe un parent
		hnodeParent = dynTable->T[i_parent];
		if (value < hnodeParent->value){//Si il faut effectivement échanger avec le parent
			while(i > 0 && value < hnodeParent->value){
				DTableSwap(dynTable, i_parent, i);
				DTableSwap(H->dict, (int)((int*)((HNode*)dynTable->T[i_parent])->ptr), (int)((int *) ((HNode*)dynTable->T[i])->ptr));

				i = i_parent;
				i_parent = floor((i - 1) / 2.0);
				hnodeParent = dynTable->T[i_parent];
			}

			return; //Pas besoin d'aller dans les autres conditions, on s'arrête
		}
	}

	while (i_fg < dynTable->used){//Tant que l'on a au moins un fg
		hnodeFG = dynTable->T[i_fg];

		if (i_fd < dynTable->used){//Si on a fg et fd
			hnodeFD = dynTable->T[i_fd];
			if (hnodeFG->value <= hnodeFD->value && value > hnodeFG->value){//Si min(fg, fd) = fg et que value > fg
				DTableSwap(dynTable, i_fg, i);
				DTableSwap(H->dict, ((int*)((HNode*)dynTable->T[i_fg])->ptr), ((int *) ((HNode*)dynTable->T[i])->ptr));
				i = i_fg;
			}
			else if (hnodeFG->value > hnodeFD->value && value > hnodeFD->value){//Si min(fg, fd) = fd et que value > fd
				DTableSwap(dynTable, i_fd, i);
				DTableSwap(H->dict, ((int*)((HNode*)dynTable->T[i_fd])->ptr), ((int *) ((HNode*)dynTable->T[i])->ptr));
				i = i_fd;
			}
			else return;
		}
		else{//Si on a que fg
			if (value > hnodeFG->value){
				DTableSwap(dynTable, i, i_fg);
				DTableSwap(H->dict, ((int*)((HNode*)dynTable->T[i_fg])->ptr), ((int *) ((HNode*)dynTable->T[i])->ptr));
				i = i_fg;
			}
			else return;
		}

		i_fg = 2*i + 1, 
		i_fd = i_fg + 1;
	}
}

void viewDTHeap(const Heap* H, void (*ptrf)(const void*)) {//Applique ptrf sur chaque élément du tableau, donc sur chaque HNode
	DTable * dynTable = H->heap;

	viewDTable(dynTable, ptrf);
}

/**********************************************************************************
 * COMPLETE BINARY TREE HEAP
 **********************************************************************************/

void* CBTHeapInsert(Heap *H, int value, void *data) {
	HNode * hNode = newHNode(value,data);

	CBTree * Tree = H->heap;	
	CBTreeInsert(Tree, hNode);
	TNode * it = Tree->last;
	HNode * hNParent;

	if(it->parent)
		hNParent = it->parent->data;

	while(it->parent && hNParent->value > value){//Erreur si value < parent->value
		CBTreeSwap(Tree, it->parent, it);
		if (it->parent)
			hNParent = it->parent->data;
	}

	return it;
}

HNode * CBTHeapExtractMin(Heap *H) {
	CBTree * Tree = H->heap;

	if (Tree->numelm == 0) return NULL;

	CBTreeSwapRootLast(Tree);

	HNode * removed = CBTreeRemove(Tree);

	if (Tree->numelm == 0) return removed;//Si il ne reste plus rien dans l'arbre, on s'arrête car inutile

	TNode * iterator = Tree->root;

	HNode * hNodeFG, *hNodeFD;
	HNode *hNodeIterator = iterator->data;

	if (iterator->left)
		hNodeFG = iterator->left->data;

	while(iterator->left && hNodeFG->value < hNodeIterator->value){//Tant qu'il y a au moins 1 enfant et que la valeur du fg est + petite que iterator
		if (iterator->right){//Il y a les 2 enfants
			hNodeFG = iterator->left->data;
			hNodeFD = iterator->right->data;

			if (hNodeFG->value > hNodeFD->value){//Si fg > fd
				CBTreeSwap(Tree, iterator, iterator->right);
			}
			else{//Si fd > fg
				CBTreeSwap(Tree, iterator, iterator->left);
			}
		}
		else{//I n'y a que le fg
			CBTreeSwap(Tree, iterator, iterator->left);
		}
	}

	return removed;
}

void CBTHeapIncreasePriority(Heap *H, void *tnode, int value) {
	CBTree * T = H->heap;
	TNode * node = tnode;
	HNode * hnode = node->data;

	TNode * tNodePr, * tNodeFg, * tNodeFd;
	HNode * hNodePr, * hNodeFg, * hNodeFd;

	hnode->value = value;
	
	if (node->parent && ((HNode*)node->parent->data)->value > value){
		tNodePr = node->parent;
		hNodePr = tNodePr->data;
		while (tNodePr && hNodePr->value > value){
			CBTreeSwap(T, tNodePr, node);
			
			if ((tNodePr = node->parent) != NULL)//On prends le nouveau parent tout en regardant si il existe pour assigner (ou non) le nouveau hNodePr
				hNodePr = tNodePr->data;
		}
	}

	while (node->left){//Tant qu'on a au moins un fils
		if (node->right){//2 fils
			tNodeFg = node->left;
			tNodeFd = node->right;
			hNodeFg = tNodeFg->data;
			hNodeFd = tNodeFd->data;

			if (hNodeFg->value <= hNodeFd->value && value > hNodeFg->value)///Si le fg est le + petit et qu'il faut échanger
				CBTreeSwap(T, node, tNodeFg);
			else if (value > hNodeFd->value)//Le fd est donc le + petit, faut-il échanger?
				CBTreeSwap(T, node, tNodeFd);
			else break;//Si pas d'échanges possibles, on arrête la boucle
		}
		else{//que un fils gauche
			if (((HNode*)node->left->data)->value < value)
				CBTreeSwap(T, node, node->left);
			else break;//Si pas d'échange possible avec l'unique fg, on arrête la boucle
		}
	}
}

void viewCBTHeap(const Heap *H, void (*ptrf)(const void*)) {//J'ai choisi le preorder pour afficher la racine en premier
	//Dans un ordre préfixé 0(Root, FG, FD)
	CBTree * T = H->heap;

	viewCBTree(T, ptrf, 0);
}

/**********************************************************************************
 * ORDERED-LIST HEAP
 **********************************************************************************/

void* OLHeapInsert(Heap *H, int value, void* data) {
	HNode * hNode = newHNode(value, data);

	List* L = H->heap;
	LNode * iterator = L->head;
	LNode * listHead = iterator;
	
	if (L->head == NULL){
		listInsertFirst(L, hNode);
		return L->head;
	}
	else{
		HNode * dataIterator = iterator->data;
		int go = 1;

		while(go){
			if (dataIterator->value >= value)//Si on a trouvé où insérer
				go = 0;
			else{
				iterator = iterator->suc;
				if(iterator)//Si l'iterator existe bien, donc on est encore dans la liste
					dataIterator = iterator->data;
				else //Sinon, on mets go à 0 pour s'arrêter
					go = 0;
			}
		}

		if ( iterator == NULL){ //Ajout en queue
      		listInsertLast(L, hNode);
			return L->tail;
		}
    	else if ( iterator == listHead ){//Ajout en Tête
      		listInsertFirst(L, hNode);
			return L->head;
		}
    	else{
        	listInsertAfter(L, hNode, iterator->pred);//iterator nous dit l'elmlist suivant, donc on insère avant celui là
			return iterator->pred;
		}
	}
}

HNode * OLHeapExtractMin(Heap *H) {
	List* L = H->heap;

	if (L->numelm == 0) return NULL;

    LNode * Lhead = listRemoveFirst(L);

    return Lhead->data;
}

void OLHeapIncreasePriority(Heap *H, void* lnode, int value) {
    List * L = H->heap;//La liste représentant le tas
    LNode * node = lnode;//L'élem list à modifier
    HNode * hnode = node->data;//L'élem de tas à modifier (contenu dans lnode)

    HNode * hNodeSuc;//le hNode successeur de lnode
    if (node->suc)
        hNodeSuc = node->suc->data;
    else
        hNodeSuc = NULL;

    HNode * hNodePred;//le hNode prédecesseur de lnode
    if (node->pred)
        hNodePred = node->pred->data;
    else
        hNodePred = NULL;



    if (value > hnode->value){//On se déplace vers la droite dans la liste
        hnode->value = value;//On assigne la nouvelle priorité à hnode (contenu dans lnode)
        while(node->suc && hnode->value > hNodeSuc->value){//Tant que la valeur suc est + grande que celle de lnode
            listSwap(L, node, node->suc);
            if (node->suc)//Vérif pour éviter une segfault(dans le cas ou après un swap, lnode se retrouve à la fin)
                hNodeSuc = node->suc->data;
        }
    }
    else{//Sinon, on parcourt la liste vers la gauche
        hnode->value = value;//On assigne la nouvelle priorité à hnode (contenu dans lnode)
        while(node->pred &&  hNodePred->value > hnode->value){
            listSwap(L, node->pred, node);
            if (node->pred)
                hNodePred = node->pred->data;
        }
    }
}

void viewOLHeap(const Heap *H, void (*ptrf)()) {
	List * L = H->heap;

	viewList(L, ptrf);
}
