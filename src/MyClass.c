#include "MyClass.h"
#include "dijkstra.h"

JNIEXPORT void JNICALL Java_MyClass_appelDijkstra
  (JNIEnv * env, jobject obj, jstring inFileName, jstring outFileName, jstring sourceName, jint heaptype){
      char *stringInFileName = (*env)->GetStringUTFChars(env, inFileName , NULL);
      char *stringOutFileName = (*env)->GetStringUTFChars(env, outFileName , NULL);
      char *stringSourceName = (*env)->GetStringUTFChars(env, sourceName , NULL);
      int h_type = (int)heaptype;

      Dijkstra(stringInFileName, stringOutFileName, stringSourceName , h_type);
  }