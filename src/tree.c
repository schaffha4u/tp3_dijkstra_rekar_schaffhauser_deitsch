#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include "tree.h"

TNode * newTNode(void* data) {
	TNode * Nb = calloc(1, sizeof(TNode));
	Nb->data = data;
	Nb->left = NULL;
	Nb->right = NULL;
	Nb->parent = NULL;
	return Nb;
}

CBTree * newCBTree() {
	
	CBTree *t = (CBTree*)calloc(1, sizeof(CBTree));
	t->root = NULL;
	t->last = NULL;
	t->numelm = 0;

	return t;
}

static void preorder(TNode *node, void (*ptrF)(const void*)) {
	if (node != NULL) {
		ptrF(node->data);
		printf(" ");
		preorder(node->left, ptrF);
		preorder(node->right, ptrF);
	}
}

static void inorder(TNode *node, void (*ptrF)(const void*)) {
	if (node != NULL) {
		inorder(node->left, ptrF);
		ptrF(node->data);
		printf(" ");
		inorder(node->right, ptrF);
	}
}

static void postorder(TNode *node, void (*ptrF)(const void*)) {
	if (node != NULL) {
		postorder(node->left, ptrF);
		postorder(node->right, ptrF);
		ptrF(node->data);
		printf(" ");
	}
}

// order = 0 (preorder), 1 (postorder), 2 (inorder)
void viewCBTree(const CBTree* tree, void (*ptrF)(const void*), int order) {
	assert(order == 0 || order == 1 || order == 2);
	printf("nb of nodes = %d\n", tree->numelm);
	switch (order) {
		case 0:
			preorder(tree->root, ptrF);
			break;
		case 1:
			postorder(tree->root, ptrF);
			break;
		case 2:
			inorder(tree->root, ptrF);
		break;
	}
	printf("\n");
}

void CBTreeInsert(CBTree* tree, void* data) {
	TNode * newNode = newTNode(data);

	if (tree->numelm == 0){ //Si l'arbre est vide
		tree->root = newNode;
		tree->last = newNode;
	}
	else{ //Sinon, 2 cas : last est soit un fils gauche, soit un fils droit
		if (tree->root == tree->last){//Si l'arbre a un seul élément
			tree->root->left = newNode;
			newNode->parent = tree->root;
			tree->last = newNode;
		}
		else if (tree->last == tree->last->parent->left) { //Si le last est un fils gauche
			tree->last->parent->right = newNode;
			newNode->parent = tree->last->parent;
			tree->last = newNode;
		}
		else {
				TNode* iterator = tree->last;
				
				while(iterator->parent && iterator == iterator->parent->right){
					iterator = iterator->parent;
				}

				if (iterator != tree->root)
					iterator = iterator->parent->right;
			
				while(iterator->left){
					iterator = iterator->left;
				}

				iterator->left = newNode;
				newNode->parent = iterator;
				tree->last = newNode;
		}
	}
	tree->numelm += 1;
}

void * CBTreeRemove(CBTree* tree) {
	assert(tree->last != NULL);
	TNode * lastNode = tree->last;

	if (tree->numelm == 1){//Si l'arbre a un seul élément
		tree->root = NULL;
		tree->last = NULL;
		tree->numelm--;
		return lastNode->data;
	}
	else{
		if (tree->numelm == 2){
			tree->last = tree->root;
			tree->root->left->parent = NULL;
			tree->root->left = NULL;
		}
		else if (tree->numelm == 3){
			tree->last = tree->root->left;
			tree->root->right->parent = NULL;
			tree->root->right = NULL;
		}
		else{//2 choix possibles, soit le last est un fils droit (simple), soit un fils gauche
			if (tree->last == tree->last->parent->right){ //Si le last est un fils droit
				tree->last = tree->last->parent->left;
				tree->last->parent->right->parent = NULL;
				tree->last->parent->right = NULL;
			}
			else{ //Donc si le last est un fils gauche
				TNode * iterator = tree->last;

				while(iterator->parent && iterator == iterator->parent->left)
					iterator = iterator->parent;

				if (iterator != tree->root)
					iterator = iterator->parent->left;

				while(iterator->right && iterator)
					iterator = iterator->right;
				
				tree->last->parent->left = NULL;
				tree->last = iterator;
			}
		}
	}
	tree->numelm--;
	return lastNode->data;
}

void CBTreeSwap(CBTree* tree, TNode* parent, TNode* child) {
	assert(parent != NULL && child != NULL && (child == parent->left || child == parent->right));
	if (parent == tree->root)
		tree->root = child;
	if (child == tree->last)
		tree->last = parent;

	if (child == parent->left){
		parent->left = child->left;
		if (child->left)
			child->left->parent = parent;

	
		child->left = parent;

		if (child->right)
			child->right->parent = parent;
	
		if (parent->right)
			parent->right->parent = child;
	
		TNode * temp = parent->right;
		parent->right = child->right;
		
		child->right = temp;
		child->parent = parent->parent;
	}
	else{ //Donc child est le fils droit de parent
		parent->right = child->right;
		if (child->right)
			child->right->parent = parent;

		child->right = parent;

		if (child->left)
			child->left->parent = parent;
		
		parent->left->parent = child;

		TNode *temp = parent->left;
		parent->left = child->left;
		child->left = temp;
		child->parent = parent->parent;
	}

	if (parent->parent){ //Si pp existe
			child->parent = parent->parent;
			if (parent == parent->parent->left)//Si p est le fg de pp
				parent->parent->left = child;
			else //Si p est le fd de pp
				parent->parent->right = child;
		}
	else
		child->parent = NULL;
	
	parent->parent = child;
}

void CBTreeSwapRootLast(CBTree* tree) {
	if(tree->root->left != NULL){
		TNode *Aux2 = tree->root;
		if(tree->last == tree->root->left || tree->last == tree->root->right){ //si les noeuds root et last sont directement reliés
			CBTreeSwap(tree, tree->root, tree->last);
		}
		else {	
			TNode *Aux = tree->last->parent;  
			tree->last->left = tree->root->left;
			tree->last->right = tree->root->right; 

			if(tree->last->parent->right){
				tree->last->parent->right = NULL;
			}
			else {
				tree->last->parent->left = NULL;
			}

			tree->last->parent = NULL;
			tree->root->left->parent = tree->last;
			tree->root->right->parent = tree->last;
			tree->root->left = NULL;
			tree->root->right = NULL;
			
			tree->root->parent = Aux;
			if(Aux->left){
				Aux->right = tree->root;
			}
			else{ 
				Aux->left = tree->root;
			}

			tree->root = tree->last;
			tree->last = Aux2;
		}
	}
}