#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "list.h"

List * newList() {
	List * L = calloc(1, sizeof(List));
	return L;
}

void deleteList(List * L, void (*ptrF)()) {
	LNode * iterator = L->head;

	if (ptrF == NULL) {
		while (iterator) {
			LNode * current = iterator;

			iterator = iterator->suc;
			free(current);
		}
	} else {
		while (iterator) {
			LNode * current = iterator;

			(*ptrF)(&current->data);
			free(current);
		}
	}
	L->head = NULL;
	L->tail = NULL;
	L->numelm = 0;
}

void viewList(const List * L, void (*ptrF)()) {
	printf("nb of nodes = %d\n", L->numelm);
	for(LNode * iterator = L->head; iterator; iterator = iterator->suc) {
		(*ptrF)(iterator->data);
		printf(" ");
	}
	printf("\n");
}

void listInsertFirst(List * L, void * data) {
	LNode * E = calloc(1, sizeof(LNode));

	E->data = data;
	E->suc = L->head;

	if(L->head == NULL)
		L->tail = E;
	else
		L->head->pred = E;
	L->head = E;
	L->numelm += 1;
}

void listInsertLast(List * L, void * data) {
	LNode * E = calloc(1, sizeof(LNode));

	E->data = data;
	E->pred = L->tail;

	if(L->tail == NULL)
		L->head = E;
	else
		L->tail->suc = E;
	L->tail = E;
	L->numelm += 1;
}

void listInsertAfter(List * L, void * data, LNode * ptrelm) {
	LNode * E = calloc(1, sizeof(LNode));

	E->data = data;
	E->pred = ptrelm;

	if(ptrelm == L->tail)
		L->tail = E;
	else
		ptrelm->suc->pred = E;
	E->suc = ptrelm->suc;
	ptrelm->suc = E;
	L->numelm += 1;
}

LNode* listRemoveFirst(List * L) {
	assert(L->head);
	LNode* E = L->head;
	L->head = L->head->suc;
	if(L->head == NULL)
		L->tail = NULL;
    else{
		L->head->pred->suc = NULL;
		L->head->pred = NULL;
	}
	L->numelm -= 1;
	return E;
}

LNode* listRemoveLast(List * L) {
	assert(L->head);

	LNode* E = L->tail;

	if(L->head == L->tail){ //Si il n'y a qu'un seul élément
		L->head = NULL;
		L->tail = NULL;
	} else {
		L->tail = L->tail->pred;
		L->tail->suc->pred = NULL;
		L->tail->suc = NULL;		
	}

	L->numelm -= 1 ;

	return E;
}

LNode* listRemoveNode(List * L, LNode * node) {
	LNode* iterator = L->head;

	while(iterator != node){
		iterator = iterator->suc;
	}

	if (iterator == L->head)
		iterator = listRemoveFirst(L);
	else if (iterator == L->tail)
		iterator = listRemoveLast(L);
	else {
		iterator->pred->suc = iterator->suc;
		iterator->suc->pred = iterator->pred;
		iterator->suc = NULL;
		iterator->pred = NULL;
		L->numelm -= 1;
	}

	return iterator;
}

void listSwap(List * L, LNode * left, LNode *right) {
	assert(left->suc == right && left == right->pred);
	
	if (left->pred)
		left->pred->suc = right;
	if (right->suc)
		right->suc->pred = left;

	left->suc = right->suc;
	right->pred = left->pred;
	left->pred = right;
	right->suc = left;

	if (left == L->head)
		L->head = right;

	if (right == L->tail)
		L->tail = left;
}