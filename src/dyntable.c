#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "dyntable.h"

DTable * newDTable() {
	DTable * T = (DTable*)calloc(1, sizeof(DTable));
	T->size = 1;
	T->used = 0;
	T->T = (void**)calloc(1, sizeof(void*));
	return T;
}

/**
 * @brief
 * Dédoubler la taille du tableau dtab
 */
static void scaleUp(DTable *dtab) {
	dtab->T = realloc(dtab->T, sizeof(void*) * (dtab->size) * 2);
	dtab->size *= 2;
}

/**
 * @brief
 * Diviser par 2 la taille du tableau dtab
 */
static void scaleDown(DTable *dtab) {
	dtab->T = realloc(dtab->T, sizeof(void*) * ((dtab->size)/2));
	dtab->size = (dtab->size) / 2;
}

void viewDTable(const DTable *dtab, void (*ptrf)(const void*)) {
	int i;
	printf("size = %d\n", dtab->size);
	printf("used = %d\n", dtab->used);
	for (i = 0; i < dtab->used; i++) {
		ptrf(dtab->T[i]);
		printf(" ");
	}
	printf("\n");
}

void DTableInsert(DTable *dtab, void *data) {
	if(dtab->used == dtab->size){
		scaleUp(dtab);
	}
	dtab->T[dtab->used] = data;
	(dtab->used)++;
}

void * DTableRemove(DTable *dtab) {
	assert(dtab->used > 0);

	void * last = dtab->T[(dtab->used)-1];

	if (dtab->used == (dtab->size) / 4)
		scaleDown(dtab);

	dtab->used -= 1;
	return last;
}

void DTableSwap(DTable *dtab, int pos1, int pos2) {
	assert(pos1 < dtab->used);
	assert(pos2 < dtab->used);

	void* temp = dtab->T[pos1];
	dtab->T[pos1] = dtab->T[pos2];
	dtab->T[pos2] = temp;
}