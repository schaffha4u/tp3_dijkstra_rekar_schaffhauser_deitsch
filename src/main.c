#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include "dyntable.h"
#include "tree.h"
#include "list.h"
#include "heap.h"
#include "town.h"
#include "road.h"
#include "graph.h"
#include "test.h"

/**
 * Exemple d'une fonction qui affiche le contenu d'un HNode.
 * A modifier si besoin.
 */
void viewHNode(const HNode *node) {
	struct town * town = node->data;
	printf("(%d, %s)", node->value, getTownName(town));
}

void viewValue(const HNode * hnode){
	printf("%d |", hnode->value);
}

void viewData(const HNode * hnode){
	int * data = hnode->data;
	printf("%d |", *(int *)data);
}

void viewInt(int *a){
	printf("%d |", (int*)a);
}

int main() {
	Dijkstra("data/map2", "data/out", "Metz",0);

	return EXIT_SUCCESS;
}