#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include "list.h"


static int compare_lists(List *l1, int l2[], int size) {
	if (l1->numelm != size)
		return 0;

	LNode *curr = l1->head;
	int i = 0;
	while (curr != NULL) {
		if ((*(int*)curr->data) != l2[i])
			return 0;
		curr = curr->suc;
		i++;
	}

	curr = l1->tail;
	i = size-1;
	while (curr != NULL) {
		if ((*(int*)curr->data) != l2[i])
			return 0;
		curr = curr->pred;
		i--;
	}
	return 1;
}

void test_list() {
	int t = 0;
	int f = 0;

	fprintf(stderr,"Testing list functions ");

	int *i1 = malloc(sizeof(int));
	int *i2 = malloc(sizeof(int));
	int *i3 = malloc(sizeof(int));
	int *i4 = malloc(sizeof(int));
	*i1 = 1;
	*i2 = 2;
	*i3 = 3;
	*i4 = 4;

	int tab[4] = {*i1,*i2,*i3,*i4};
	int t1[1] = {*i1};
	int t2[2] = {*i2,*i1};
	int t3[3] = {*i3,*i2,*i1};
	int t4[4] = {*i4,*i3,*i2,*i1};

	List *L = newList();
	listInsertLast(L, (int*) i1);
	if (compare_lists(L, tab, 1) == 0) {fprintf(stderr,"x");f++;} else {fprintf(stderr,".");t++;}
	listInsertLast(L, (int*) i2);
	if (compare_lists(L, tab, 2) == 0) {fprintf(stderr,"x");f++;} else {fprintf(stderr,".");t++;}
	listInsertLast(L, (int*) i3);
	if (compare_lists(L, tab, 3) == 0) {fprintf(stderr,"x");f++;} else {fprintf(stderr,".");t++;}
	listInsertLast(L, (int*) i4);
	if (compare_lists(L, tab, 4) == 0) {fprintf(stderr,"x");f++;} else {fprintf(stderr,".");t++;}

	deleteList(L, NULL);
	if (compare_lists(L, tab, 0) == 0) {fprintf(stderr,"x");f++;} else {fprintf(stderr,".");t++;}

	listInsertFirst(L, (int*) i1);
	if (compare_lists(L, t1, 1) == 0) {fprintf(stderr,"x");f++;} else {fprintf(stderr,".");t++;}
	listInsertFirst(L, (int*) i2);
	if (compare_lists(L, t2, 2) == 0) {fprintf(stderr,"x");f++;} else {fprintf(stderr,".");t++;}
	listInsertFirst(L, (int*) i3);
	if (compare_lists(L, t3, 3) == 0) {fprintf(stderr,"x");f++;} else {fprintf(stderr,".");t++;}
	listInsertFirst(L, (int*) i4);
	if (compare_lists(L, t4, 4) == 0) {fprintf(stderr,"x");f++;} else {fprintf(stderr,".");t++;}
}