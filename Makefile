IDIR = include
ODIR = obj
SDIR = src
JDIR = java

CC = gcc
CFLAGS = -g -Wall -fPIC -I$(IDIR)
EXE = -o $(JDIR)/libexemple.so

JINCLUDES = -I/usr/lib/jvm/jdk1.8.0_251/ -I/usr/lib/jvm/jdk1.8.0_251/include/ -I/usr/lib/jvm/jdk1.8.0_251/include/linux/

_DEPS = dijkstra.h MyClass.h dyntable.h tree.h list.h heap.h town.h road.h graph.h 
DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS))

_OBJ = dijkstra.o dyntable.o tree.o list.o heap.o town.o road.o graph.o 
OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))

all: $(ODIR)/MyClass.o $(OBJ)
	$(CC) $(CFLAGS) -shared $(EXE) $^

$(ODIR)/MyClass.o : $(SDIR)/MyClass.c $(DEPS)
	$(CC) $(CFLAGS) $(JINCLUDES) -c -o $@ $<

$(ODIR)/%.o: $(SDIR)/%.c $(DEPS)
	$(CC) $(CFLAGS) -c -o $@ $<

.PHONY: clean

clean:
	rm -f $(ODIR)/*.o $(JDIR)/libexemple.so
