#ifndef _DJIKSTRA_H_
#define _DJIKSTRA_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include "dyntable.h"
#include "tree.h"
#include "list.h"
#include "heap.h"
#include "town.h"
#include "road.h"
#include "graph.h"

void Dijkstra(char * inFileName, char * outFileName, char * sourceName, int heaptype);

void saveDijkstra(graph G, char * outFileName, char * sourceName);

void viewSolution(graph G, char * sourceName);

#endif