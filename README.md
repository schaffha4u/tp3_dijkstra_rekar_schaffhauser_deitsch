# Projet de Synthèse (Algorithme de Dijkstra)
Bienvenue sur le dépôt du projet réalisé pour le cours de Projet de Synthèse en L2 Informatique, @Metz.

### Prérequis

* JavaJDK 
* JavaFX
* git/make/gcc

### Installation

```
git clone https://gitlab.com/schaffha4u/tp3_dijkstra_rekar_schaffhauser_deitsch.git
```
```
cd tp3_dijkstra_rekar_schaffhauser_deitsch
```
```
cd java
```
```
javac -h . MyClass.java
```
```
mv MyClass.h ../include
```
```
javac ControllerFenetre.java Main.java Segment.java Sommet.java
```
```
cd ..
```
```
make
```
```
cd java
```
```
java -Djava.library.path=. Main
```
### Problèmes possibles à l'installation
* Le jdk doit être le JDK 8 de Oracle (pour contenir javafx)
* En cas de probème avec JNI au lancement du Makefile, modifier les chemins vers le JDK dans le makefile

### Créateurs

* **Bastien SCHAFFHAUSER**
* **Axel REKAR**
* **Thibaut DEITSCH**
